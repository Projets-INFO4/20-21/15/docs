# Project 15: Retrocomputing

## Project Overview
This project is about re-exploring past computer hardware. In particular, our objective is to study the functioning of old processors. As we will not work with actual hardware, we will use the Python language to simulate a processor. We are also making use of the software [Digital](https://github.com/hneemann/Digital) which enables us to easily design combinational circuits. Our objective is to implement the emulated processor as an actual component in Digital.

You can find a more in-depths description of the project on the [AIR Wiki](https://air.imag.fr/index.php/Retrocompute_simulateur).

Documentation for MC6809 processor:
- [6809 Emulation Page](http://atjs.mbnet.fi/mc6809/#Infohttp://atjs.mbnet.fi/mc6809/#Info)
- [6809 Datasheet](http://pdf.datasheetcatalog.net/datasheet/motorola/MC68B09S.pdf)

### Code
Code for this project can be found [here](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/15/retrocomputing).

### Charts

TCP Server-Client Sequence diagram between Digital and the MC6809 processor: <img src="charts/tcp_sequence_diagram.png" alt="sequence diagram showing a TCP communication between the Digital client and the MC6089 processor's server" width="75%">


The original diagram can be viewed online on [Lucidchart](https://lucid.app/lucidchart/invitations/accept/bf30edc6-0a77-48ab-a774-546f74bd7a70).

### Screenshot
The following combinational circuit (made with Digital) can interact with a MC6809 Server and execute code on the remote microprocessor.
<img src="img/DemoCircuit.png" alt="Combinational circuit made with Digital that can execute code on a remote MC6809 Server" width="75%">

## Our Team
We are a group of three INFO4 students from Polytech Grenoble:
- Sami ELHADJI TCHIAMBOU
- Corentin HUMBERT
- Mathis MUTEL

## follow-up
- **[02/01/21]** We dedicated this session to the discovery of maven and the creation of custom components in digital. 
Maven allows us to easily use "jar" files. It's quite convenient for the custom components of digital. We installed Maven and tried to understand the customComponent example of digital.

- **[02/08/21]** We started studying the functioning of the MC6809 Python emulator. We began by setting up the environment with poetry and then ran tests. Next time we will try to set up a simple TCP tunnel between a Python executable and a Java process in order to lay the foundation for our future implementation of the MC6809 in Digital. 

- **[02/22/21]** Nous avons fait fonctioner ensemble des serveurs et clients Java vers Python et Python vers Java. De plus nous avons commencé à réfléchir sur la facon dont nous transmetrons les informations (à poursuivre). Et nous avons commencé à lire la Datasheet du MC6809.

- **[02/23/21]** Nous avons continué notre lecture de la datasheet et du code python relatif au simulateur. Cela a occuppé une grande majorité de la séance. Mathis a égalemment réalisé le diagramme de séquence pour la connexion client serveur et les échanges entre les 2.

- **[01/03/21]** This time, we started working on different parts of the project:
    - Sami kept on reading the MC6089 datasheet to learn more about the processor's architecture and functioning.
    - Corentin played around with the MC6809 Python simulator and created a new file [sandbox.py](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/15/retrocomputing/-/blob/coco_proc/MC6809/sandbox.py) in which he created a small program to add two numbers. The purpose was to learn about the processor by learning about new instructions and how the different ways they could be used.
    - Mathis started working on the Digital side and began to think about how we could create a component that would allow Digital to communicate with the MC6809 emulated in Python. This will be achieved by setting up a TCP Server-Client connection between Python and Java.
    <br/>
- **[02/03/21]** Travail effectué durant la séance :
    - Sami : Lecture de la doc Digital, et début de l'initialisation des broches du composant sur Digital
    - Corentin : Analyse des instructions du processeur 6809 et mise en place du serveur Python
    - Mathis : Définition du contenu de la trame des messages TCP, et initialisation de la partie client du composant
    <br/>

- **[08/03/21]** Travail effectué durant la séance :
    - Sami et Mathis : Création des points de liaison côté Digital
    - Corentin : Création des points de liaison (DigitalPin) côté MC6809 et implémentation de la méthode process_digins
    <br/>

- **[09/03/21]** Travail effectué durant la séance :
    - Sami : ajout du bus de donnée et tentative de restructuration du component pers
    - Mathis et Corentin: reflexion implementation/couplage avec digital. 
    - On envisage de restructurer ou modifier digital voire le code du 6809 en python.
    <br/>

- **[15/03/21]** Travail effectué durant la séance :
    - étude du composant multi ram afin de s'en inspiré pour notre composant
    - aprofondissement de la compréhention du fonctionnement de Digital et demande d'information au créateur du logiciel
    - discussion sur le fonctionnement de notre composant coté Digital
    <br/>

- **[16/03/21]** Travail effectué durant la séance :
    - Test de connexion TCP entre le simulateur 6809 et Digital
    - Approfondissement de la compréhention du fonctionnement de Digital et étude des bus bidirectionels
    - Etude du fonctionnement du processeur pour préparer le contrôle à distance via un client Digital
    <br/>

- **[22/03/21]** Travail effectué durant la séance :
    - composant opperationel mais rudimentaire. Reste plus qu'à l'améliorer
    - encore une fois, lecture de code et tests divers sur Digital
    - Piste d'implémentation d'une nouvelle mémoire côté python adapté aux échanges TCP avec Digital
    <br/>

- **[23/03/21]** Travail effectué durant la séance :
    - suite de l'implémentaion de la partie client  du composant Digital
    - modification du composant Digital
    - avancement d'implémentation d'une nouvelle mémoire côté python adapté aux échanges TCP avec Digital
    <br/>

- **[01/04/21]** Travail effectué durant la journée :
    - Finalisation de l'implémentation côté Digital, le serveur Python peut désormais exécuter du code processeur stocké dans une mémoire côté client Digital
    - Réalisation d'un circuit combinatoire dans Digital permettant d'exécuter du code sur le processeur distant
    - Test finale avec un programme réalisant l'addition de deux chiffres
    - Rédaction du rapport
    <br/>

